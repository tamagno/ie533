function ak = zf(Mz, zk)

%% Zero Forcing
%           -----    nk          ------            ------
%    ak--->| M(z)|---+--->zk--->|1/M(z)|--->xk--->|slicer|--->âk
%           -----                ------            ------

xk = filter(1, Mz, zk);               % Zero Forcing Equalization.

ak = zeros(1, length(zk));            % Allocate the output system.

for ii = 1 : length(zk)                 
  if(xk(ii) > 0) ak(ii) = 1; else ak(ii) = -1; end  % Apply the slice decision.
end

end