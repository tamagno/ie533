function ak = fde(Mz, zk)

%% Zero Forcing
%           -----    nk              ------
%    ak--->| M(z)|---+--->zk---+--->|slicer|---->âk
%           -----              |-    ------    |
%                              |               | 
%                              |               |
%                              |     ------    |
%                              |<---|M(z)-1|<---
%                                    ------    


ak    = zeros(1, length(zk));           % Allocate the output system.
fifo  = zeros(length(Mz)-1, 1);         % Create the fifo.
mk    = Mz(2:end);                      % Create the inverse system M(z)-1.

if(zk(1) > 0) ak(1) = 1; else ak(1) = -1; end % Apply the slice decision of the first element.
 
for ii = 2 : length(zk)
  
  fifo(1)   = ak(ii-1);              % Insert the previous slice output at the inverse system;
  
  ak(ii) = zk(ii) - (mk*fifo);       % Remove the ISI from the new symbol;
  
  if(ak(ii) > 0) ak(ii) = 1; else ak(ii) = -1; end % Apply the slice decision.
  
  %%fifo = shift(fifo,1);                 % Shift the fifo to right.(OCTAVE)
  fifo(2:end) = fifo(1:end-1);            % Shift the fifo to right.(MatLab)
  
end
end