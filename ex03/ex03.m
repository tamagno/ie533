clear; close all; clc;
%pkg load communications



%% Adjustable Parameters

N   = 10^5;           % Number of symbols
Mz  = [1 0.5 0.2 0.1];% M(z)
SNR = 0:2:20;           % Signal to noise ratio (dB)

%% Hard Parameters
errorZF = zeros(1,length(SNR));
errorFDE = zeros(1,length(SNR));
alphabet = [-1 1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% System Model 
%           -----          nk    
%    ak--->| M(z)|--->yk---+--->zk
%           -----       

fprintf('Executing for %d symbols... Wait!',N);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Symbol Generations

ak = randsrc(1, N, alphabet);             % 2-PAM

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% System

yk = filter(Mz, 1, ak);                 % Filter with the system model M(z)

for ii=1:length(SNR)

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Noise

  zk = awgn(yk, SNR(ii), 'measured', 'dB');   % Add awgn noise
  %zk = yk;                                   % Add no noise

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Zero-Forcing

  akZF = zf(Mz, zk);


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% FDE

  akFDE = fde(Mz, zk);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% VITERBI
  preamble = [1 0];  
  akVit = mlseeq([preamble zk],Mz,alphabet,10,'rst',1,preamble,[]);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Error Estimation
  
  errorZF(ii) = sum(akZF~=ak);
  errorFDE(ii) = sum(akFDE~=ak);
  errorVIT(ii) = sum(real(akVit(length(preamble)+1:end))~=ak);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot Items
figure
semilogy(SNR, errorZF./N);
hold;
semilogy(SNR, errorFDE./N);
semilogy(SNR, errorVIT./N);
xlabel ('Eb/N0 (dB)');
ylabel ('Bit Error ');
hold off;
legend('ZF','FDE','Viterbi')

%Debug
% fprintf('ZF errors: ');
% errorZF./N
% fprintf('FDE errors: ');
% errorFDE./N
% fprintf('Viterbi errors: ');
% errorVIT./N





