function [upsampled_vector] = upsample (vector, number_of_samples)

  upsampled_vector = zeros(1,length(vector)*number_of_samples);

  for i = 0 : length(vector) - 1  
    upsampled_vector((i*number_of_samples)+1) = vector(i+1);
  endfor

endfunction