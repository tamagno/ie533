clear all; clc all; close all;

pkg load communications;


%------------------------------------------------
% PARAMETERS
%------------------------------------------------

  
T = 2;            	% Symbol period
oversample = 100; 	% Period oversample
n = 5;            	% Number of T symbol periods for the pulse shape
input_size = 128;  	% Number of bits to be transmited
fc = 22;	  	      % Modulation frequency (Hz). This value should be less than half of the frequency sampling given by oversample/T.
alpha = 0;	  	    % Roll off factor
SNR = 3;		        % Noise spectral density
alphabet = [1+j -1+j -1-j 1-j]; % Alphabet

% Choose what to plot
sel_plots = [ 
        1  % 1 TX - Constellation
        1  % 2 TX - Pulse shape
        1  % 3 TX - Base band inphase and quadrature signal
        1  % 4 TX - Eye diagram 
        1  % 5 TX - Modulated signal before the transmission
        1  % 6 RX - Modulated signal after the transmission
        1  % 7 RX - Base band inphase and quadrature signal with noise
        1  % 8 RX - Base band inphase and quadrature after match filter
        1  % 9 RX - Eye diagram
        1  % 10 RX - Constellation
        ];
%------------------------------------------------
% TX
%------------------------------------------------

% Generate the simulation time
t = -T*n/2: T/oversample : (T*n/2)-T/oversample;

% Random bit vector
x = rand(1, input_size) > 0.5;
%x = [ 1 0 0 1 1 1];

% Map to 4-QAM symbols
symbols = mapper(x);

% Pulse Shape
g = raisedcos(t, T, alpha);

% Upsampling
upsamples = upsample(symbols, oversample);

% Apply the pulse shapes
tx0 = conv(upsamples, g, "same");

% Update the time sample to match the convolution size
t = 0 : T/oversample : (length(tx0)*(T/oversample))-T/oversample;

% Modulate the signals
txm_i = real(tx0) .* ((2^(1/2)).*cos(2*pi*fc*t));
txm_q = imag(tx0) .* ((2^(1/2)).*sin(2*pi*fc*t));

txm = txm_i - txm_q;


%------------------------------------------------
% CHANNEL
%------------------------------------------------

% Send the signal through the channel
rxm = awgn(txm, SNR);

%------------------------------------------------
% RX
%------------------------------------------------

% Down converter
rx_noise_i = rxm .* ((2^(1/2)).*cos(2*pi*fc*t));
rx_noise_q = rxm .* -((2^(1/2)).*sin(2*pi*fc*t));

rx_noise = rx_noise_i + rx_noise_q * j;

%% Apply the match filter.
rx0 = conv(rx_noise, g, "same"); 

% Downsampling
downsamples = rx0(1:oversample:length(rx0));

%Demapper
y = demapper(downsamples);

% BER
BER = (sum(y==x) - length(x))/length(x)

% Call the plots
plots (sel_plots, symbols, oversample, g, tx0, txm, rxm, rx_noise_i, rx_noise_q, rx0, downsamples);





