function [symbols] = mapper (x)
  
  % Define the number of bits
  nbits = 2;

  % Define the number of symbols
  symbols = zeros(1, length(x)/nbits);

  for ii = 0 : length(symbols)-1
    switch(x(((nbits*ii)+1):((nbits*ii)+2)))
      case([0 0])
        symbols(ii+1) = 1+j;
      case([0 1])
        symbols(ii+1) = -1+j;
      case([1 1])
        symbols(ii+1) = -1-j;
      case([1 0])
        symbols(ii+1) = 1-j;
     endswitch
  endfor
 
endfunction
