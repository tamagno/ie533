function [y] = demapper (downsamples)
  
  % Define the number of bits
  nbits = 2;

  % Define the number of symbols
  y = zeros(1, length(downsamples)*nbits);

  for ii = 0 : length(downsamples)-1
    if(real(downsamples(ii+1)) > 0)
      if(imag(downsamples(ii+1)) > 0)
        y((ii*nbits)+1:(ii*nbits)+2) = [0 0];
      else 
        y((ii*nbits)+1:(ii*nbits)+2) = [1 0];
      endif
    else
     if(imag(downsamples(ii+1)) > 0)
        y((ii*nbits)+1:(ii*nbits)+2) = [0 1];
     else 
        y((ii*nbits)+1:(ii*nbits)+2) = [1 1];
     endif
    endif
  endfor
 
endfunction
