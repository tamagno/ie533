function [y] = raisedcos (t, T, alpha)
  
    if(alpha == 0)
      y = sinc(t,T);
    else
      for ii = 1:length(t)
        if(t(ii) == T/(2*alpha) || t(ii) == -T/(2*alpha))
          y(ii) = sinc(t(ii),T) .* pi/4;
        else
          y(ii) = sinc(t(ii),T) .* (cos((alpha.*pi.*t(ii))./T)./(1-((2.*alpha.*t(ii))./T).^2));
        endif
      endfor
    endif
  
  
endfunction