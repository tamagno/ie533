function [y] = sinc (t, T)
 
  for ii = 1:length(t)
    if(t(ii) == 0)
      y(ii) = 1;
    else
      y(ii) = (sin(pi.*(t(ii)./T)))./(pi.*(t(ii)./T));
     endif
  endfor
 
endfunction