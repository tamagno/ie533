function plots (sel_plots, symbols, oversample, g, tx0, txm, rxm, rx_noise_i, rx_noise_q, rx0, downsamples)


	% Constellation diagram
	if sel_plots(1) == 1
	  figure('name', 'TX Constellation diagram');
	  plot(symbols, "*", "markersize", 10);
	  title("Constellation Diagram");
	  axis([-2 2 -2 2]);
	endif

	% Pulse Shape
	if sel_plots(2) == 1
	  figure('name','Pulse shape - Raised Cosine');
	  plot(g);
	  axis([0 length(g) -1 2]);
	  title("Pulse Shape");
	  xlabel("Samples");
	  ylabel("Amplitude");
	endif

	% Plot the Base band inphase and quadrature signal
	if sel_plots(3) == 1 
    figure('name','Base band inphase and quadrature signal on TX') ;
	  hold;
	  subplot(2,1,1);
    plot(real(tx0));
    xlabel("Samples");
	  ylabel("Amplitude");
    title("Base band inphase on RX");
    legend("inphase");
    %axis([0 length(rx_noise_i) -2 2]);
    subplot(2,1,2);
    plot(imag(tx0),'r');
    xlabel("Samples");
	  ylabel("Amplitude");
	  title("Base band quadrature signal on RX");
    legend("quadrature");
	  %axis([0 length(rx_noise_q) -2 2]);
  	hold off;
	endif
	
	% Plot the eye diagram
	if sel_plots(4) == 1
	  eyediagram(tx0, oversample, oversample);
	endif

	% Plot the modulated signal
	if sel_plots(5) == 1
	  figure('name', 'TX Modulated Signal');
	  plot(txm);
	  title("TX Modulated Signal");
	  xlabel("Samples");
	  ylabel("Amplitude");
	endif

	% Plot the modulated signal after the transmission
	if sel_plots(6) == 1
	  figure('name', 'RX Modulated Signal');
	  plot(rxm);
	  title("RX Modulated Signal");
	  xlabel("Samples");
	  ylabel("Amplitude");
	endif

	% Inphase and quadrature signal after the transmission
	if sel_plots(7) == 1
	  figure('name','Base band inphase and quadrature signal on RX') ;
	  hold;
	  subplot(2,1,1);
    plot(rx_noise_i);
    xlabel("Samples");
	  ylabel("Amplitude");
    title("Base band inphase on RX");
    legend("inphase");
    %axis([0 length(rx_noise_i) -2 2]);
    subplot(2,1,2);
    plot(rx_noise_q,'r');
    xlabel("Samples");
	  ylabel("Amplitude");
	  title("Base band quadrature signal on RX");
    legend("quadrature");
  	hold off;
	endif

  % Inphase and quadrature signal after the transmission
	if sel_plots(8) == 1
	  figure('name','Base band inphase and quadrature signal on RX after match filter') ;
	  hold;
	  subplot(2,1,1);
    plot(real(rx0));
    xlabel("Samples");
	  ylabel("Amplitude");
    title("Base band inphase on RX");
    legend("inphase");
    %axis([0 length(rx0_i) -2 2]);
    subplot(2,1,2);
    plot(imag(rx0),'r');
    xlabel("Samples");
	  ylabel("Amplitude");
	  title("Base band quadrature signal on RX");
    legend("quadrature");
  	  hold off;
	endif
 
	% Plot the eye diagram
	if sel_plots(9) == 1
	  eyediagram(rx0, oversample, oversample);
	endif
  
  % Constellation diagram
	if sel_plots(10) == 1
	  figure('name', 'RX Constellation diagram');
	  plot(downsamples, "*", "markersize", 10);
	  title("Constellation Diagram");
	endif
endfunction
