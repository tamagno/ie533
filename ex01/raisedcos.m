function [y] = raisedcos (t, T, alpha)
 
  for ii = 1:length(t)
    if(t(ii) == 0)
      y(ii) = 1;
    else
      y(ii) = (sin(pi.*(t(ii)./T)))./(pi.*(t(ii)./T)) .* (cos(alpha.*pi.*t(ii)./T)./(1-(2.*((alpha.*t(ii)./T).^2))));
     endif
  endfor

endfunction