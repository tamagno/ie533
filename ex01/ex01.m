clc all;
clear all;

% Adjustable parameters
n = 64;                     % Number of bits
f = 10;                     % Symbol frequency
oversample = 100;           % Samples per symbol period
pam = 8;                    % PAM compression 
alpha = 0.7;                % Roll off [0 1]


ts_over = (1/f)/oversample; % Oversample period
t = -1/f:ts_over:1/f;       % Time interval

% Generate the random data.
%x = rand(1,n) > 0.5;
x = [0   1   1   0   1   0   1   1   0   1   0   1   0   0   0   1   1   0   1   0   0   1   0   0   1   0   1   1   0   0   0   0   1   1   0   1   1   0   0   0   0   0   0   0   1   0   1   0   0   1   1   0   0   1   0   1   1   0   0   0   1   1   1   1];

% Map the random data to a K-PAM
x_mapped = mapper(x,pam);

% Create the pulses
square_pulse = ones(1,oversample);
gaussian_pulse = gaussmf(t, [.25*1/f 0]);
raised_cos_pulse = raisedcos(t, 1/f, alpha);
  
% Oversampling the signal
over_sampled_x = upsample(x_mapped, oversample);

% Convolution of the pulses and mapped signal
y_square    = conv(over_sampled_x, square_pulse);
y_gaussian  = conv(over_sampled_x, gaussian_pulse);
y_raisedcos = conv(over_sampled_x, raised_cos_pulse);

% Create the plots

figure(1);
subplot(3,1,1);
plot(y_square);
title("Square Pulse");
ylabel("Amplitude");
xlabel("Samples");

subplot(3,1,2);
plot(y_gaussian);
title("Gaussian Pulse");
ylabel("Amplitude");
xlabel("Samples");

subplot(3,1,3);
plot(y_raisedcos);
title("Raised Cosin Pulse");
ylabel("Amplitude");
xlabel("Samples");
figure(1);

print figure1.jpeg