function [symbols] = mapper (bitstream, pam)
  
  % Define the number of bits
  bits = log2(pam);
  
  % Define the number of symbols
  symbols = zeros(1, length(bitstream)/bits);
  
  % Define the buffer for conversion 
  buffer = zeros(1,bits);
  
  
  for i=0 : (length(symbols)-1)
    
    buffer = bitstream(i*bits+1:i*bits+bits);
    buffer = fliplr(buffer);
    

    if(buffer(bits) == 0) % Negative amplitude
      
      amplitude = -((2^bits)-1);
      
      for j = 1 : bits-1
        if(buffer(j) == 1)
          amplitude = amplitude + 2^j;
        endif
      endfor
      
    else  % Positive amplitued
      
      amplitude = 1;
      
      for j = 1 : bits-1
        if(buffer(j) == 1)
          amplitude = amplitude + 2^j;
         endif
      endfor
      
    endif
    
    symbols(i+1) = amplitude;
    
  endfor
  
  
endfunction
